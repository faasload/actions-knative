# Knative actions for dataset generation using FaaSLoad

This repository contains cloud functions ("actions") for Knative.
They are prepared for usage with FaaSLoad, i.e. they use the expected set of parameters and are written to use a Swift storage backend.

## Quick start

You should use these actions as directed by FaaSLoad's documentation on its workload injector, or dataset generator modes.
Nonetheless, you must prepare the actions as follows:

 1. set the authentication URL of your Swift storage in "swift\_parameters.json"
 2. build the functions: run `make` in the main folder (assuming you have installed the build dependencies)

"swift\_parameters.json" is used to set default values to a few parameters of the actions related to the Swift storage.

**Hint for Swift storage:** you can deploy the Swift backend storage using the yaml file in the repository:

```sh
kubectl apply swift-deploy.yml
```

The image for the pod is based on the following image ["morrisjobke/docker-swift-onlyone"](https://hub.docker.com/r/morrisjobke/docker-swift-onlyone) which is the one used when using FaaSLoad with Apache Openwhisk

In addition, the script "load\_sample\_inputs\_swift.py" loads sample inputs for all actions to the Swift storage (reading the configuration in "swift\_parameters.json") with a naming scheme that fits FaaSLoad's generator mode.
Note that you need the Swift client installed (package python3-swift-client on Ubuntu Server 20.04), as well as jq (a JSON parser for the shell).

```sh
pip3 install python-swiftclient # To install swift
sudo apt install jq # to install jq on ubuntu servers
```

Inputs are stored in "samples", under folders corresponding to their kind (image, audio, video...).
They are checkout into Git using LFS, so before running the script "load\_sample\_inputs\_swift.py", you must pull them:

```sh
git lfs pull
```

Besides when using swift inside Kubernetes (with Knative or not) port forwarding of the service is needed before running the script "load\_sample\_inputs\_swift.py" in order to access or change the content of the pod from the host. This is not needed when swift is accessed from inside the cluster.

```sh
kubectl port-forward svc/swift 8080:8080
```

## Details

Function images are built with the daemon docker from minikube hence before using the Makefile the command to load the environment variables of docker is needed `eval $(minikube -p <your_profile> docker-env)` where <your_profile> should be **faasload** by default. Functions extract data from, and load data to a Swift storage backend, and as such, they all have a dependency on a Swift client library.

Actions are deployed using FaaSLoad tooling script "load-knative-assets.py".
They are declared in a Manifest file "manifest.yml" that is read by FaaSLoad tooling.
This is actually an OpenWhisk feature, to be used with its tool wskdeploy, so you may also use the actions without FaaSLoad and deploy them all at once by calling `wskdeploy` from this folder.

The folder "src" contains action runtimes, i.e. each folder corresponds to a general runtime (Python, NodeJS...) and a set of
dependencies (such as Wand for "py\_wand"). Actions under an action runtime folder are based on the same runtime, and can be
built by the same Makefile found in the action runtime folder.

## Actions

In this section I explain what are the specific steps taken to build the different action runtimes (i.e. each subfolder of
"src"), as found in their respective Makefiles. Notes indicate how Makefiles actually slightly differ from what is shown here;
you will find more details in the Makefiles.

In a general manner, there are only one step:

 * build a runtime image: a customized Docker image embedding more software pieces: native dependencies, build dependencies,
   or the main dependency of a function for various reasons.

It is needed by Knative to build a container image (here docker because of minikube the default development platform). That is why all action folder have a Dockerfile.

## Check an action is working.
The action inside src/js_sharp/sepia will be take as an example. It considered that the build of the image with the Makefile inside minikube as well as the deployment of swift inside the cluster have already been done. 

To create an action for Knative there are 2 possibilities:

1) Using PyKnative
The librairy developped for FaaSLoad called [PyKnative](https://gitlab.com/faasload/pyknative) can create an action for knative just by creating the following python file and then run it with `python3 <your file>`:

```python
from pyknative.action import create
from pyknative.client import get_api_configuration
from pyknative.models import Action

if __name__ == "__main__":
    cfg = get_api_configuration()
    a = Action(
        name="js-sharp-sepia",
        image="dev.local/js_sharp_sepia:1.0.0",
        env=[]
    )
    create(action=a)
```

2) Using Knative resources inside Kubernetes
To create a knative service the following yaml file is needed and can be applied with `kubectl apply -f <your file>`:
```yaml
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: js-sharp-sepia
  namespace: default
spec:
  template:
    spec:
      containers:
      - image: dev.local/js_sharp_sepia:1.0.0
```

After the action has been deployed. This "simple" command is enough:

```sh
curl $(kubectl get ksvc -o=jsonpath='{range.items[*]}{.status.url}{"\n"}{end}' | grep js-sharp-sepia) \
  -d '{
    "width": 120,
    "user": "test:tester",
    "key": "testing",
    "authurl": "http://swift.default.svc.cluster.local:8080/auth/v1.0",
    "object": "3.jpg",
    "incont": "image",
    "outcont": "testtest"
  }' \
  -H 'Content-Type: application/json'
```
