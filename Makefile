# Main Makefile
#
# The target "all" will build all action runtime targets, i.e. all subfolders
# of "src". It delegates building the actions of an action runtime to specific
# Makefiles found in the action runtime subfolders.

TARGETS := $(wildcard src/*/.)
CLEAN_TARGETS := $(addsuffix .clean,$(TARGETS)) 

.PHONY: all $(TARGETS) clean $(CLEAN_TARGETS) deploy

all: $(TARGETS)

build:

# The trick with cp || true is a bit ugly, it would be better to check that the patterns match.
$(TARGETS): src/%/.: | build
	@echo Runtime target: $*
	$(MAKE) -C $@

clean: $(CLEAN_TARGETS)

$(CLEAN_TARGETS): %.clean:
	@echo Clean target: $*
	$(MAKE) -C $* clean
