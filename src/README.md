# Action runtimes

| Runtime | Processing | Language | Main dependency | Dependency package | Runtime image | Image content |
|--------:|------------|----------|-----------------|-------------------:|--------------:|--------------:|
| `js_sharp` | image | NodeJS | [Sharp](https://sharp.pixelplumbing.com/) | yes | no | N/A |

## Notes

* value "params" in "Processing" means that the functions do not fetch anything from the Swift storage backend, in other
  words, they only process their parameters;
* value "yes\*" in "Dependency package" indicates that the dependency package is only required because of the library to
  communicate with the Swift storage backend;
* column "Image content" gives the reason(s) a custom runtime image is needed:
  * "n": the action needs native dependencies;
  * "b": the action needs build dependencies;
  * "d": the main dependency of the action must be embedded in the runtime image (instead of the dependency package).
