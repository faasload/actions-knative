#!/usr/bin/env bash

SWIFT_PARAMS="swift_parameters.json"
SAMPLES_DIR="samples"
INPUT_KINDS="image:jpg audio:wav video:avi"


if ! which "jq" > /dev/null 2>&1 || ! which "swift" > /dev/null 2>&1; then
    echo "swift and jq are required in your \$PATH." >&2
    exit 2
fi

# swift_parameters.json define the parameters for the actions.
# however, the auth url is different whether the client is inside or outside the kubernetes cluster.
swift_user="$(jq -r ".user" "$SWIFT_PARAMS")"
swift_key="$(jq -r ".key" "$SWIFT_PARAMS")"
swift_authurl="$(jq -r ".authurl" "http://127.0.0.1:8080/auth/v1.0")" 

for input_kind in $INPUT_KINDS; do
    input_dir="$(echo "$input_kind" | cut -d":" -f1)"
    input_ext="$(echo "$input_kind" | cut -d":" -f2)"

    echo "Uploading $input_dir inputs with extension $input_ext"

    i=1
    
    for input in "$SAMPLES_DIR/$input_dir/"*; do
        echo -n "$i "

        swift_dir="$input_dir"

        swift --quiet\
            --auth "$swift_authurl" --user "$swift_user" --key "$swift_key"\
            upload --object-name $i."$input_ext" "$swift_dir" "$input"

        i=$(expr $i + 1)
    done

    echo
done

